<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TCA']['tx_campaignmonitor_domain_model_field'] = array(
    'ctrl' => array(
        'title' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'hideTable' => true,
        'requestUpdate' => 'type',

        'versioningWS' => 2,
        'versioning_followPages' => true,

        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'sortby' => 'sorting',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'title,value,options,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('campaignmonitor') . 'Resources/Public/Icons/tx_campaignmonitor_domain_model_field.svg',
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type, title, value, options, multiple, required',
    ),
    'types' => array(
        '1' => array('showitem' => 'type, title, value, options, multiple, required, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, starttime, endtime'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(

        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0),
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_campaignmonitor_domain_model_field',
                'foreign_table_where' => 'AND tx_campaignmonitor_domain_model_field.pid=###CURRENT_PID### AND tx_campaignmonitor_domain_model_field.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),

        't3ver_label' => array(
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ),
        ),

        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y')),
                ),
            ),
        ),

        'type' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.type',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.type.1', 'Input'),
                    array('LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.type.2', 'Email'),
                    array('LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.type.3', 'Select'),
                ),
                'size' => 1,
                'maxitems' => 1,
            ),
        ),

        'title' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.title',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required',
            ),
        ),

        'value' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.value',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required, nospace, lower',
            ),
        ),

        'options' => array(
            'exclude' => 1,
            'displayCond' => 'FIELD:type:=:Select',
            'label' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.options',
            'config' => array(
                'type' => 'text',
                'cols' => 30,
                'rows' => 5,
                'eval' => 'trim, required',
            ),
        ),

        'multiple' => array(
            'exclude' => 1,
            'displayCond' => 'FIELD:type:=:Select',
            'label' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.multiple',
            'config' => array(
                'type' => 'check',
            ),
        ),

        'required' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:campaignmonitor/Resources/Private/Language/locallang_db.xlf:tx_campaignmonitor_domain_model_field.required',
            'config' => array(
                'type' => 'check',
            ),
        ),

    ),
);
