plugin.tx_campaignmonitor {
  view {
    # cat=plugin.tx_campaignmonitor/file; type=string; label=Path to template root (FE)
    templateRootPath = EXT:campaignmonitor/Resources/Private/Templates/
    # cat=plugin.tx_campaignmonitor/file; type=string; label=Path to template partials (FE)
    partialRootPath = EXT:campaignmonitor/Resources/Private/Partials/
    # cat=plugin.tx_campaignmonitor/file; type=string; label=Path to template layouts (FE)
    layoutRootPath = EXT:campaignmonitor/Resources/Private/Layouts/
  }
  persistence {
    # cat=plugin.tx_campaignmonitor//a; type=string; label=Default storage PID
    storagePid =
  }
  settings {

  }
}