<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "campaignmonitor".
 *
 * Auto generated 16-10-2014 15:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Campaign Monitor',
    'description' => 'Campaign Monitor subscribe extension',
    'category' => 'plugin',
    'version' => '2.0.0',
    'state' => 'beta',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearcacheonload' => false,
    'author' => 'Raphael Zschorsch',
    'author_email' => 'rafu1987@gmail.com',
    'author_company' => null,
    'constraints' => array(
        'depends' => array(
            'extbase' => '7.6.0-7.6.99',
            'fluid' => '7.6.0-7.6.99',
            'typo3' => '7.6.0-7.6.99',
            'php' => '5.6.0-7.0.99',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
);
