﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


What does it do?
================

This TYPO3 extension enables you to build a simple subscribe form that integrates directly with Campaign Monitor right on your site.

The extension is featured on the Campaign Monitor homepage here: https://www.campaignmonitor.com/integrations/typo3