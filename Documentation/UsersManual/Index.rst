﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _users-manual:

Users manual
============

Just install the extension through the extension manager and include the static TypoScript to your root page and you're ready to go.

You can add the plugin on any TYPO3 page and the settings should be self-explanatory.