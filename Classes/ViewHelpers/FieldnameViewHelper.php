<?php
namespace RZ\Campaignmonitor\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 * @package Campaignmonitor
 * @subpackage ViewHelpers
 */
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class FieldnameViewHelper extends AbstractConditionViewHelper
{

    /**
     * @param string $field The field
     * @return string
     */
    public function render($field)
    {
        $prefix = 'field';

        $fieldname = strtolower($field);
        $fieldname = str_replace(array('ä', 'ö', 'ü', 'ß'), array('ae', 'oe', 'ue', 'ss'), $fieldname);
        $fieldname = str_replace(array(' ', '-', '_', ',', '.', ':', ';', '?', '&', '$', '§', '!', '%', '/', '='), '', $fieldname);
        $fieldname = ucfirst($fieldname);
        $fieldname = $prefix . $fieldname;

        return $fieldname;
    }
}
